 var data = context.getVariable("request.formparam.data");
 var validInput = true;
 var errorMessage = "";
 //print("data: ", data);
 
 if(data !== null) //make sure data param was actually passed in
 {
    if(data.length !== 0) //make sure data is not empty
    {
        var dataObj = JSON.parse(data);
        var sfdcUserId = dataObj.salesforceUserId;
        if(sfdcUserId !== null && sfdcUserId !== undefined) // && 
        {
            if(sfdcUserId.length !== 0)
            {
                var regEx = /^[0-9a-zA-Z]{18}$/; //sfdc user id must be an 18-char alphanumeric
                if(regEx.test(sfdcUserId) === false){
                    validInput = false;
                    errorMessage = "salesforceUserId is malformed. Must be 18-character alphanumeric.";
                }   
            }
            else
            {
                validInput = false;
                errorMessage = "salesforceUserId is empty.";
            }
        }
        else //sfdc user id was undefined or null or misspelled
        {
            validInput = false;
            errorMessage = "salesforceUserId is not set or misspelled.";
        }
    }
    else //data was empty
    {
        validInput = false;
        errorMessage = "data param is empty.";
    }
 }
 else{ //data was null
    validInput = false;
    errorMessage= "data param is missing in JSON.";
 }
 print("validInput: ", validInput);
 context.setVariable("validInput", validInput);
 context.setVariable("validationErrorMessage", errorMessage);